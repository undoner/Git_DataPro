/*
 * 本类作用：Timer时间类，实现定时从数据库中读取相关配置信息（暂未用到）
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.main;
import java.util.Timer;
import java.util.TimerTask;
import com.yxj.db.DbProperty;
import com.yxj.util.DataOutput;

public class DataReloadTimer {      
    Timer timer;      
    public DataReloadTimer(int seconds) {   
    	//DbConn.conn();
    	DbProperty.getInitDB();//读取db.property，并获得数据库连接
        timer = new Timer();      
        timer.schedule(new TimerTestTask(), seconds*1000,seconds*1000);      
    }      
    static class TimerTestTask extends TimerTask {      
        public void run() {      
        	DataOutput.print("自动加载配置信息", 1);  
            //DbOption_GpsCeshi.getXXX();//添加自定义方法
            //timer.cancel();      
        }      
    }      
    public static void main(String args[]) {      
        System.out.println("Prepare to schedule task.");      
        new DataReloadTimer(1);      
        System.out.println("Task scheduled.");      
    }      
}  