/*
 * 本类作用：数据库操作：初始化连接池
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.yxj.util.DataOutput;

public class DbConn {
	private static ComboPooledDataSource cpds = null;

	public static void init() {
		String drivers = System.getProperty("drivers");
		String url = System.getProperty("url");
		String user = System.getProperty("user");
		String password = System.getProperty("password");
		int Initial_PoolSize = Integer.parseInt(System.getProperty("Initial_PoolSize"));
		int Min_PoolSize = Integer.parseInt(System.getProperty("Min_PoolSize"));
		int Max_PoolSize = Integer.parseInt(System.getProperty("Max_PoolSize"));
		int Acquire_Increment = Integer.parseInt(System
				.getProperty("Acquire_Increment"));
		int Idle_Test_Period = 14400;// 4h
		int maxIdleTime = 18000;// 5h
		int acquireRetryAttempts = 3;
		try {
			cpds = new ComboPooledDataSource();
			cpds.setDriverClass(drivers);
			cpds.setJdbcUrl(url); // 数据库URL
			cpds.setUser(user); // 数据库用户名
			cpds.setPassword(password); // 数据库密码
			cpds.setInitialPoolSize(Initial_PoolSize); // 初始连接池大小
			cpds.setMinPoolSize(Min_PoolSize); // 最小连接数
			cpds.setMaxPoolSize(Max_PoolSize); // 最大连接数
			cpds.setAcquireIncrement(Acquire_Increment); // 连接数的数量
			cpds.setIdleConnectionTestPeriod(Idle_Test_Period);// 每隔Idle_Test_Periods测试连接有效性
			cpds.setMaxIdleTime(maxIdleTime);// 最大空闲时间,maxIdleTime秒内未使用则连接被丢弃。
			cpds.setAcquireRetryAttempts(acquireRetryAttempts);// 定义在从数据库获取新连接失败后重复尝试的次数acquireRetryAttempts
		} catch (Exception e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}

	}

	public static Connection getConnection() {
		Connection connection = null;
		try {
			if (cpds == null) {
				init();// 初始化数据库连接池
			}
			connection = cpds.getConnection(); // 获得连接数
		} catch (SQLException ex) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ ex.getMessage(), 2);
		}
		return connection;
	}

	public static void release() {
		try {
			if (cpds != null) {
				cpds.close();
			}
		} catch (Exception ex) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ ex.getMessage(), 2);
		}
	}
	
	public static void conn() {
		Connection c = null;
		Statement conn = null;
		String drivers = System.getProperty("drivers");
		String url = System.getProperty("url");
		String user = System.getProperty("user");
		String password = System.getProperty("password");
		Properties props = System.getProperties();
		try {
			Class.forName(drivers).newInstance();
			c = DriverManager.getConnection(url, user, password);
			conn = c.createStatement();
			props.put("conn", conn);
			System.setProperties(props);
		} catch (SQLException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		} catch (InstantiationException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		} catch (IllegalAccessException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		} catch (ClassNotFoundException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}
	}
}
