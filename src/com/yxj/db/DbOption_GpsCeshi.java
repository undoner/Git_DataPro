/*
 * 本类作用：业务数据的数据库存储（上一步：业务数据解析，下一步：处理结束）
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import com.yxj.util.DataOutput;

public class DbOption_GpsCeshi {

	public static void add(String str_sql,long dataID){
		long start=System.currentTimeMillis();
		if(str_sql!=null)
		{	
			try {
				Connection connection=DbConn.getConnection();
				Statement st=connection.createStatement();
				System.err.println("新建连接");
				st.addBatch(str_sql);
				st.executeBatch();
				st.close();
				connection.close();
				System.err.println("关闭连接");
			} catch (SQLException e) {
				StackTraceElement[] stacks = new Throwable().getStackTrace();  
		        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
		        DataOutput.print(error +":"+ e.getMessage(), 2);
			}
			long end=System.currentTimeMillis();
			System.err.println(dataID+"DbOption Cost Time:"+(end-start));
		}
		
	}

	//从数据库中加载相关配置信息
	public static boolean getDataAlart()
	{
		DataOutput.print("加载完毕！", 1);  
		return true;
	}

}
