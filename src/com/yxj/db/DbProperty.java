/*
 * 本类作用：数据库操作：读取配置文件
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

import com.yxj.util.DataOutput;


public class DbProperty {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Properties props=new Properties();
		try {
			String filePath= System.getProperty("user.dir")+"\\configuration\\db.property";
			BufferedReader bw = new BufferedReader(new FileReader(filePath));
			String line = null;
			while((line = bw.readLine()) != null){
				if(!line.startsWith("#"))
					props.setProperty(line.trim().split("=")[0].trim(), line.trim().split("=")[1].trim());
			}
			bw.close();
		} catch (IOException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}		
		System.setProperties(props);
		String drivers=System.getProperty("drivers");
		String url=System.getProperty("url");
		String user=System.getProperty("user");
		String password=System.getProperty("password");
		String Initial_PoolSize=System.getProperty("Initial_PoolSize");
		String Max_PoolSize=System.getProperty("Max_PoolSize");

		System.out.println(Initial_PoolSize);
		//getDBPro();
	}

	public static Connection getInitDB()
	{
		String filePath = System.getProperty("user.dir") + File.separator
				+ "configuration" + File.separator + "db.property";
		Properties props=System.getProperties();
		if(props.get("conn")!=null)
			return (Connection)props.get("conn");
		try {
			BufferedReader bw = new BufferedReader(new FileReader(filePath));
			String line = null;
			while((line = bw.readLine()) != null){
				if(!line.startsWith("#"))
					props.setProperty(line.trim().split("=")[0].trim(), line.trim().split("=")[1].trim());
			}
			bw.close();
		} catch (IOException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}	
		System.setProperties(props);
		return DbConn.getConnection();
	}

}
