/*
 * 本类作用：业务数据解析（上一步：数据接收DataRecMain，下一步：业务数据存储DbOption_GpsCeshi）
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.analyse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.yxj.db.DbOption_GpsCeshi;
import com.yxj.util.DataOutput;

public class Data_GpsCeshi {

	public static void main(String[] args) {	
		String data="13862082455:117.12775:029.78209:143.2136";
	}

	//数据解析
	//13862082455:117.12775:029.78209:143.2136
	public static void analyseData(String data,long dataID) {
		String str_sql=";";
		data =data.trim();
		String dataTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
		String site=null;	//站点
		String time=dataTime;	//时间
		String gpsinfo="";	//
		String latitude=null;	//纬度
		String longitude=null;	//经度
		try{
			for (int i = 0; i < data.split(":").length; i++) {
				switch (i) {
				case 0:
					site = data.split(":")[i];
					break;
				case 1:
					longitude= data.split(":")[i];
					break;
				case 2:
					latitude = data.split(":")[i];
					break;
				case 3:
					gpsinfo = data.split(":")[i];
					break;
				}
			}
		}
			catch(Exception e)
			{
				StackTraceElement[] stacks = new Throwable().getStackTrace();  
		        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
		        DataOutput.print(error +":"+ e.getMessage(), 2);
			}
			str_sql="insert into gpsdataceshi (time,site,longitude,latitude,gpsinfo) values ('"+time+"','"+site+"','"+gpsinfo+"','"+latitude+"','"+longitude+"')";
			System.out.println(str_sql);
			DbOption_GpsCeshi.add(str_sql,dataID);
	}



	//测试用
	public static List<String> analyseData1(String data) {

		data =data.trim();
		List<String> result =new ArrayList<String>();
		String[] al={"","","","","","","","","","",""};
		result.clear();
		String site=data.substring(0, 3);
		String time=null;	//时间
		String gpsinfo=null;	//gpsinfo
		String datalist=null;	//数据集合
		String latitude=null;	//纬度
		String longitude=null;	//经度
		System.out.println("site"+site);
		System.out.println("t.length()"+data.length());
		System.out.println("data="+data);
		try{
			if(true)
			{
				int datalen =data.split(site+":").length;
				for (int i = 0; i < datalen; i++) {
					switch (i) {
					case 1:
						al[1] = data.split(site+":")[i];
						break;
					case 2:
						al[2] = data.split(site+":")[i];
						break;
					case 3:
						al[3] = data.split(site+":")[i];
						break;
					case 4:
						al[4] = data.split(site+":")[i];
						break;
					case 5:
						al[5] = data.split(site+":")[i];
						break;
					case 6:
						al[6] = data.split(site+":")[i];
						break;
					case 7:
						al[7] = data.split(site+":")[i];
						break;
					case 8:
						al[8] = data.split(site+":")[i];
						break;
					case 9:
						al[9] = data.split(site+":")[i];
						break;
					case 10:
						al[10] = data.split(site+":")[i];
						break;
					}
				}
				for(int k=0;k<al.length;k++)
				{

					if(!al[k].isEmpty())
					{
						System.out.println(datalen+":al["+k+"]"+al[k]);
						for (int i = 0; i < al[k].split(":").length; i++) {
							switch (i) {
							case 0:
								latitude = al[k].split(":")[i];
								break;
							case 1:
								longitude = al[k].split(":")[i];
								break;
							case 2:
								datalist = al[k].split(":")[i];
								break;
							}

						}
						time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
						gpsinfo=datalist.substring(0,8);
						System.out.println("latitude:"+latitude+" longitude:"+longitude+" datalist:"+datalist+" gpsinfo:"+gpsinfo);

						//此处在静态方法中引用外部静态变量listsql,会有线程安全问题
						//listsql.add("call sp_insertData('"+time+"','"+site+"','"+gpsinfo+"','"+latitude+"','"+longitude+"')");
						result.add("\n"+" time="+time+" site:"+site+" latitude="+latitude+" longitude="+longitude+" gpsinfo="+gpsinfo);
						//DbOption_GpsCshi.add(listsql);
					}
				}
			}}
		catch(Exception e)
		{
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}
		return result;
	}


}