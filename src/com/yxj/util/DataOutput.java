/*
 * 本类作用：日志输出（可改用log4j等）
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import com.yxj.main.DataRecMain;

public class DataOutput  {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	//控制台及面板信息输出:1为数据信息，2为异常信息
	public static void print(String info,int flag)
	{
		try
		{
			if(flag==1)//输出信息
			{
				//textArea.append("\n"+info);
				DataRecMain.setTextArea(info);
				System.out.println(info);
				DataOutput.outfile(info);
			}
			if(flag==2)//输出异常信息
			{
				//textArea.append("\n"+info);
				//DataRecMain.setTextArea(info);
				System.err.println(info);
				DataOutput.outfile(info);
			}
		}
		catch(Exception e)
		{
			System.out.println("disData.Outinfo:"+e.getMessage());
			//System.err.println(e.getMessage());

		}
	}
	public static void outfile(String filecontent)
	{
		try{
			//File file = new File("./log/"+filename);//在系统当前目录下建立/log/目录
			File file = new File(System.getProperty("user.dir") +"/log/"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".log");
			//File file = new File("d:"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".log");
			if (!file.exists())
				file.createNewFile();
			//System.out.println(file.getAbsolutePath());

			FileOutputStream out;
			out = new FileOutputStream(file, true);
			//OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8"); 
			OutputStreamWriter osw = new OutputStreamWriter(out, "GBK"); 
			osw.write('\n');
			osw.write(filecontent);
			osw.close();
		}
		catch(Exception e)
		{
			System.err.println("DataOutput:"+e.getMessage());
		}
	}

}
