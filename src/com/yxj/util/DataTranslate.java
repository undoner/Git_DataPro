/*
 * 本类作用：工具类：进制转换
 * 
 * 作者：袁小杰
 * 博客：http://blog.csdn.net/undoner
 * GIT：https://git.oschina.net/undoner
 * QQ：15137281
 * 
 */
package com.yxj.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public class DataTranslate {
	
	public static void main(String args[]) throws UnsupportedEncodingException
	{
		String str="中国人";
		Charset charSet=Charset.forName("utf-8");
		CharsetEncoder encoder=charSet.newEncoder();				
		ByteBuffer echoBuffer = ByteBuffer.allocate(1024);
		try {
			echoBuffer=encoder.encode(CharBuffer.wrap(str));
		} catch (CharacterCodingException e) {
			StackTraceElement[] stacks = new Throwable().getStackTrace();  
	        String error="class: "+stacks[1].getClassName()+"; method: "+stacks[1].getMethodName()+"; number: "+stacks[1].getLineNumber();
	        DataOutput.print(error +":"+ e.getMessage(), 2);
		}
		int a_echoBuffer=1;
		if (a_echoBuffer > 0) {
			ByteBuffer bf = ByteBuffer.allocate(26);
			byte[] b = echoBuffer.array();
			byte[] f = new byte[a_echoBuffer];
			for (int i = 0; i < a_echoBuffer; i++) {
				f[i] = b[i];
			}
		}
		System.out.println(echoBuffer.toString());
		String str1="中国人";
		String str0=new String(str1.getBytes("UTF-8"),"UTF-8");
		System.out.println(str0);
		String str2="中国人";
		System.out.println(getASCII(str0.getBytes("UTF-8")));
		System.out.println(getASCII(str2.getBytes()));
	}
	
	public static String getASCII(byte[] bt)
	{
		String data="";
		ByteBuffer bb = ByteBuffer.allocate (bt.length);
	    bb.put (bt);
	    bb.flip ();
	    Charset cs = Charset.forName ("UTF-8");
	    CharBuffer cb = cs.decode (bb);
	    cb.array();
	    for(int j=0;j<cb.array().length;j++)
	    	data+=cb.array()[j];
		return  data;
	}
	
	public static String getHex(byte b)
	{
		String HexString=Integer.toString(b& 0xFF, 16);
		if(HexString.length()==1)
			HexString="0"+HexString;
		return HexString.toUpperCase();
		
	}
	  /** 
     *  
     * @param hexString 
     * @return 十六進制字符串 转换为byte數組
     */
	public static byte[] hexStringToBytes(String hexString) {  
	    if (hexString == null || hexString.equals("")) {  
	        return null;  
	    }  
	    hexString = hexString.toUpperCase();  
	    int length = hexString.length() / 2;  
	    char[] hexChars = hexString.toCharArray();  
	    byte[] d = new byte[length];  
	    for (int i = 0; i < length; i++) {  
	        int pos = i * 2;  
	        d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));  
	    }  
	    return d;  
	}

	private static int charToByte(char c) {
		// TODO Auto-generated method stub
		return (byte) "0123456789ABCDEF".indexOf(c);
	}  
	  /** 
     *  
     * @param src 
     * @return 转换为十六進制字符串 
     */  
	public static String bytesToHexString(byte[] src){  
	    StringBuilder stringBuilder = new StringBuilder("");  
	    if (src == null || src.length <= 0) {  
	        return null;  
	    }  
	    for (int i = 0; i < src.length; i++) {  
	        int v = src[i] & 0xFF;  
	        String hv = Integer.toHexString(v);  
	        if (hv.length() < 2) {  
	            stringBuilder.append(0);  
	        }  
	        stringBuilder.append(hv);  
	    }  
	    return stringBuilder.toString().toUpperCase();  
	} 
	
	  /** 
     *  
     * @param str 
     * @return 转换为二进制字符串 
     */  
    public static String bytes2BinaryStr(byte[] bArray){  
    	 String[] binaryArray =   
    	       {"0000","0001","0010","0011",  
    	       "0100","0101","0110","0111",  
    	       "1000","1001","1010","1011",  
    	       "1100","1101","1110","1111"};
        String outStr = "";  
        int pos = 0;  
        for(byte b:bArray){  
            //高四位  
            pos = (b&0xF0)>>4;  
            outStr+=binaryArray[pos];  
            //低四位  
            pos=b&0x0F;  
            outStr+=binaryArray[pos];  
        }  
        return outStr;  
          
    } 

    public static  byte[] getBytes (char[] chars) {
    	   Charset cs = Charset.forName ("UTF-8");
    	   CharBuffer cb = CharBuffer.allocate (chars.length);
    	   cb.put (chars);
    	                 cb.flip ();
    	   ByteBuffer bb = cs.encode (cb);
    	   return bb.array();
    	 }
    
	public static int bytesToInt(byte[] intByte) {

	int fromByte = 0;

	 for (int i = 0; i < 2; i++)
	 {
	 int n = (intByte[i] < 0 ? (int)intByte[i] + 256 : (int)intByte[i]) << (8 * i);
	 System.out.println(n);
	 fromByte += n;
	 }
	 return fromByte;
	}

}
